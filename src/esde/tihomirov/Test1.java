package esde.tihomirov;

public class Test1 {
    private int first;
    private int second;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int sum(){
        return this.first + this.second;
    }

    public int max(){
        return Math.max(this.first, this.second);
    }
}
