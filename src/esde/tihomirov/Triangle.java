package esde.tihomirov;

public class Triangle {
    static class Side{
        double length;

        public Side(double length) {
            this.length = length;
        }

        public double getLength() {
            return length;
        }

        public void setLength(double length) {
            this.length = length;
        }
    }

    private final Side sideA;
    private final Side sideB;
    private final Side sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = new Side(sideA);
        this.sideB = new Side(sideB);
        this.sideC = new Side(sideC);
    }

    public double perimeter(){
        return this.sideA.getLength() + this.sideB.getLength() + this.sideC.getLength();
    }
    
    public double square(){
        double p = this.perimeter() / 2;
        return Math.sqrt(p*(p-sideA.getLength())*(p-sideB.getLength())*(p-sideC.getLength()));
    }
}
