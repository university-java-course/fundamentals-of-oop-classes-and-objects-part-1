package esde.tihomirov;

public class Time {
    private int seconds;
    private int minutes;
    private int hours;

    public Time(int seconds, int minutes, int hours) {
        this.setSeconds(seconds);
        this.setMinutes(minutes);
        this.setHours(hours);
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes >= 0 && minutes <= 59 ? minutes : 0;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours >= 0 && hours <= 24 ? hours : 0;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds >= 0 && seconds <= 60 ? seconds : 0;
    }
}
