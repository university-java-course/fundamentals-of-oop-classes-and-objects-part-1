package esde.tihomirov;

public class Counter {
    private int value;
    private int min;
    private int max;

    public void increase() {
        this.value++;
    }

    public void increase(int value) {
        this.value += value;
    }

    public void decrease() {
        this.value--;
    }

    public void decrease(int value) {
        this.value -= value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public Counter(int value) {
        this.value = value;
        this.min = Integer.MIN_VALUE;
        this.max = Integer.MAX_VALUE;
    }

    public Counter(int value, int min, int max) {
        this.value = value;
        this.min = min;
        this.max = max;
    }

    public Counter(){
        this.value = 0;
        this.min = Integer.MIN_VALUE;
        this.max = Integer.MAX_VALUE;
    }
}
